import React from 'react';
import {Cookies} from 'react-cookie';
import {ToastContainer, toast} from 'react-toastify';
import { withRouter } from 'react-router-dom'

const cookie = new Cookies();
const AuthContext = React.createContext();

class AuthProvider extends React.Component {
    state = { isAuth: false };
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }
    componentDidMount(){
        if (cookie.get('token') !== undefined) {
            this.setState({ isAuth: true });
            this.props.history.push('/dashboard');
        }else{
            this.setState({ isAuth: false });
        }
    }
    login() {
        // setting timeout to mimic an async login
        setTimeout(() => {
            cookie.set('token', 'arfs');
            toast.success('Log In Success!', { autoClose: 7000 });
            this.setState({ isAuth: true });
            this.props.history.push('/dashboard');
        }, 1000)
    }
    logout() {
        cookie.remove('token');
        this.setState({ isAuth: false });
        toast.success('Log Out Success!', { autoClose: 7000 });
        this.props.history.push('/');
    }

    render() {
        return (
            <AuthContext.Provider
                value={{
                    isAuth: this.state.isAuth,
                    login: this.login,
                    logout: this.logout
                }}
            >
                <ToastContainer />
                {this.props.children}
            </AuthContext.Provider>
        )
    }
}

const AuthConsumer = AuthContext.Consumer;
const AuthProviderWithRouter = withRouter(AuthProvider);
export {AuthProviderWithRouter, AuthConsumer}