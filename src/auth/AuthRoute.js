import React from 'react';
import {AuthConsumer} from './AuthContext';
import {Redirect, Route} from 'react-router-dom';


const AuthRoute = ({ component: Component, ...rest }) => (
    <AuthConsumer>
        { isAuth => {
           return <Route
                render={
                    props =>
                        isAuth.isAuth
                            ? <Component {...props} />
                            : <Redirect to="/"/>
                }
                {...rest}
            />
        }}
    </AuthConsumer>
)

export default AuthRoute;