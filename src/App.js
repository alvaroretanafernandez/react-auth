import React, {Component} from 'react';
import './App.css';
import {AuthProviderWithRouter} from './auth/AuthContext';
import Header from './header/Header';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {createMuiTheme} from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/purple';
import {MuiThemeProvider} from '@material-ui/core/styles';
import Dashboard from './dashboard/dashboard';
import Home from './home/home';
import AuthRoute from './auth/AuthRoute';
import {CookiesProvider} from 'react-cookie';
import 'react-toastify/dist/ReactToastify.css';
const theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: {
            main: '#ab47bc',
        },
    },
    typography: {
        useNextVariants: true,
    },
});


class App extends Component {
    render() {
        return (
            <div className="App">
                <CookiesProvider>
                    <BrowserRouter>
                        <AuthProviderWithRouter>
                            <MuiThemeProvider theme={theme}>
                                <Header/>
                                <Switch>
                                    <AuthRoute path="/dashboard" component={Dashboard}/>
                                    <Route path="/" component={Home}/>
                                </Switch>
                            </MuiThemeProvider>
                        </AuthProviderWithRouter>
                    </BrowserRouter>
                </CookiesProvider>
            </div>
        );
    }
}

export default App;
