import React, {Component} from 'react';

class Dashboard extends Component {
    render() {
        return (
            <div>
                <h2>User Dashboard</h2>
                <h3>You are now authenticated!</h3>
            </div>
        )
    }
}
export default Dashboard