import React from 'react';
import { Link } from 'react-router-dom';
import './header.scss';
import {AuthConsumer} from '../auth/AuthContext';
import AppBar from '@material-ui/core/es/AppBar/AppBar';
import Toolbar from '@material-ui/core/es/Toolbar/Toolbar';
import Typography from '@material-ui/core/es/Typography/Typography';
import Button from '@material-ui/core/es/Button/Button';

export default () => (
    <header>
        <AuthConsumer>
            {({isAuth, login, logout }) => {
               return <div className="nav">
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="h6" color="inherit" className="subnav">
                                FREE POWDER
                            </Typography>
                            {isAuth ? (
                                <div>
                                    <Button color="inherit" >
                                        <Link color="inherit" to="/dashboard">  Dashboard</Link>
                                    </Button>
                                    <Button color="inherit" onClick={logout}>Logout</Button>
                                </div>
                            ) : (
                                <Button color="inherit" onClick={login}>Login</Button>
                            )}
                        </Toolbar>
                    </AppBar>


                </div>
            }}
        </AuthConsumer>
    </header>
)

